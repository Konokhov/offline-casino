<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x41a;&#x430;&#x437;&#x438;&#x43d;&#x43e;" FOLDED="false" ID="ID_155424656" CREATED="1542396781648" MODIFIED="1542400304574" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3" RULE="ON_BRANCH_CREATION"/>
<node TEXT="&#x420;&#x443;&#x43b;&#x435;&#x442;&#x43a;&#x430;" POSITION="right" ID="ID_324888738" CREATED="1542398292219" MODIFIED="1542400300282" HGAP_QUANTITY="44.749999083578615 pt" VSHIFT_QUANTITY="-49.49999852478509 pt">
<edge COLOR="#ff0000"/>
<node TEXT="&#x43d;&#x430;&#x43f;&#x438;&#x441;&#x430;&#x442;&#x44c; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" ID="ID_925043616" CREATED="1542398338930" MODIFIED="1542485362416"/>
<node TEXT="&#x43d;&#x430;&#x439;&#x442;&#x438; &#x442;&#x430;&#x431;&#x43b;&#x438;&#x446;&#x443; &#x441;&#x442;&#x430;&#x432;&#x43e;&#x43a;" ID="ID_269154252" CREATED="1542485365823" MODIFIED="1542485376946"/>
</node>
<node TEXT="&#x41a;&#x43b;&#x430;&#x441;&#x441;&#x438;&#x447;&#x435;&#x441;&#x43a;&#x438;&#x439; &#x438;&#x433;&#x440;&#x43e;&#x432;&#x43e;&#x439; &#x430;&#x432;&#x442;&#x43e;&#x43c;&#x430;&#x442;" POSITION="left" ID="ID_348096306" CREATED="1542398358499" MODIFIED="1542400304573" HGAP_QUANTITY="53.74999881535772 pt" VSHIFT_QUANTITY="-8.999999731779106 pt">
<edge COLOR="#0000ff"/>
<node TEXT="&#x43d;&#x430;&#x439;&#x442;&#x438; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" ID="ID_1534334419" CREATED="1542485388616" MODIFIED="1542485442061"/>
<node TEXT="&#x43f;&#x43e;&#x43d;&#x44f;&#x442;&#x44c; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" ID="ID_11206671" CREATED="1542485428585" MODIFIED="1542485438630"/>
<node TEXT="&#x43d;&#x430;&#x43f;&#x438;&#x441;&#x430;&#x442;&#x44c; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" ID="ID_1435402464" CREATED="1542485443274" MODIFIED="1542485451056"/>
</node>
<node TEXT="&#x411;&#x43b;&#x435;&#x43a; &#x414;&#x436;&#x435;&#x43a;" POSITION="right" ID="ID_412851390" CREATED="1542398363466" MODIFIED="1542485378761" HGAP_QUANTITY="46.24999903887513 pt" VSHIFT_QUANTITY="35.99999892711642 pt">
<edge COLOR="#00ff00"/>
<node TEXT="&#x43d;&#x430;&#x43f;&#x438;&#x441;&#x430;&#x442;&#x44c; &#x430;&#x43b;&#x433;&#x43e;&#x440;&#x438;&#x442;&#x43c;" ID="ID_603426144" CREATED="1542401096740" MODIFIED="1542485385546"/>
</node>
</node>
</map>
